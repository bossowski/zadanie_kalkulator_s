import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatFormFieldModule, MatDatepickerModule, MatSelectModule, MatButtonModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { SalaryService } from './salary/salary.service';
import { SalaryComponent } from './salary/salary.component';
import { DatepikerComponent } from './datepiker/datepiker.component';

@NgModule({
  declarations: [
    AppComponent,
    SalaryComponent,
    DatepikerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatSelectModule,
    MatButtonModule
  ],
  providers: [SalaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
