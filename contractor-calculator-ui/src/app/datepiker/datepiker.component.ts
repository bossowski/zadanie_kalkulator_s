import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import * as moment from 'moment';

@Component({
  selector: 'app-datepiker',
  templateUrl: './datepiker.component.html',
  styleUrls: ['./datepiker.component.css']
})
export class DatepikerComponent {
  @Input() parentFormGroup: FormGroup;
  today = moment();
  maxDate = moment();
}
