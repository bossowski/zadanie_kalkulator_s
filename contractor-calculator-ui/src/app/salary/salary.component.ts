import { Component, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { SalaryService } from './salary.service';

@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.css']
})
export class SalaryComponent {

  netAmount: number;
  salaryForm: FormGroup;
  countries = [
    { name: 'Germany', countryCode: 'de' },
    { name: 'Poland', countryCode: 'pl' },
    { name: 'United Kingdom', countryCode: 'gb' }
  ];

  constructor(private fb: FormBuilder, private salaryService: SalaryService) {
    this.createForm();
  }

  createForm() {
    this.salaryForm = this.fb.group({
      countryCode: ['', Validators.required],
      invoiceDate: ['', Validators.required],
      days: ['', Validators.required],
      grossDailyRate: ['', Validators.required]
    });
  }

  onSubmit() {
    const formModel = this.salaryForm.value;
    this.salaryService.calculateNetAmount(
      formModel.countryCode,
      formModel.invoiceDate.startOf('day').format('YYYY-MM-DD'),
      formModel.days,
      formModel.grossDailyRate
    ).subscribe(response => {
      this.netAmount = response;
    });
  }
}
