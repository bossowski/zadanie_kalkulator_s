import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SalaryService {

  constructor(private http: HttpClient) { }

  calculateNetAmount(countryCode: string, invoiceDate: string, days: number, grossDailyRate: number): Observable<any> {
    return this.http.get(`/salaries/countries/${countryCode}/invoiceDates/${invoiceDate}/days/${days}/grossDailyRates/${grossDailyRate}`);
  }
}
