# zadanie_kalkulator_s
Contractor calculator project (Spring Boot 2, Angular 5, Java 9)

# Setup
mvn -N io.takari:maven:wrapper

# Build
./mvnw clean install

# Run
cd contractor-calculator-rest && ../mvnw spring-boot:run

# Usage
Open: `http://localhost:8080`

