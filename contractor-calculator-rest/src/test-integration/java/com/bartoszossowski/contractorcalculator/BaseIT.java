package com.bartoszossowski.contractorcalculator;

import com.bartoszossowski.contractorcalculator.config.Application;
import com.bartoszossowski.contractorcalculator.config.TestConfig;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Application.class, TestConfig.class})
public abstract class BaseIT {
}
