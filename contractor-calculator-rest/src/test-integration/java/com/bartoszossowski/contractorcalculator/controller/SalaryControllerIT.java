package com.bartoszossowski.contractorcalculator.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;

import com.bartoszossowski.contractorcalculator.BaseIT;
import com.bartoszossowski.contractorcalculator.config.exception.ExchangeRateServiceUnavailableException;
import com.bartoszossowski.contractorcalculator.domain.Country;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static com.bartoszossowski.contractorcalculator.util.TestValueGeneratorUtil.randomInt;
import static com.bartoszossowski.contractorcalculator.util.TestValueGeneratorUtil.randomPositiveBigDecimal;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

public class SalaryControllerIT extends BaseIT {

    @Autowired
    private SalaryController salaryController;

    @Autowired
    private RestTemplate restTemplate;
    private MockRestServiceServer mockServer;

    @Before
    public void createMessageTemplates() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void shouldCalculateNetAmountForPoland() {
        //Given
        Country country = Country.POLAND;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 10;
        BigDecimal grossDailyRate = new BigDecimal("238");

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("800"));
    }

    @Test
    public void shouldCalculateNetAmountForUnitedKingdom() {
        //Given
        Country country = Country.UNITED_KINGDOM;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 10;
        BigDecimal grossDailyRate = new BigDecimal("500");

        mockPolishExchangeRateService(country.getCurrencyCode(), invoiceDate.minusDays(1), new BigDecimal("4.7207"));

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("16050.38"));
    }

    @Test
    public void shouldCalculateNetAmountForGermany() {
        //Given
        Country country = Country.GERMANY;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 10;
        BigDecimal grossDailyRate = new BigDecimal("500");

        mockPolishExchangeRateService(country.getCurrencyCode(), invoiceDate.minusDays(1), new BigDecimal("4.1854"));

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("14090.86"));
    }

    @Test
    public void shouldCalculateNetAmountWithOnlyFixedCostForPolandAndNoWorkingDays() {
        //Given
        Country country = Country.POLAND;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 0;
        BigDecimal grossDailyRate = randomPositiveBigDecimal();

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("-1200"));
    }

    @Test
    public void shouldCalculateNetAmountWithOnlyFixedCostForUnitedKingdomAndNoWorkingDays() {
        //Given
        Country country = Country.UNITED_KINGDOM;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 0;
        BigDecimal grossDailyRate = randomPositiveBigDecimal();

        mockPolishExchangeRateService(country.getCurrencyCode(), invoiceDate.minusDays(1), BigDecimal.ONE);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("-600"));
    }

    @Test
    public void shouldCalculateNetAmountWithOnlyFixedCostForGermanyAndNoWorkingDays() {
        //Given
        Country country = Country.GERMANY;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 0;
        BigDecimal grossDailyRate = randomPositiveBigDecimal();

        mockPolishExchangeRateService(country.getCurrencyCode(), invoiceDate.minusDays(1), BigDecimal.ONE);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("-800"));
    }

    @Test
    public void shouldCalculateNetAmountWithProperVatForPoland() {
        //Given
        Country country = Country.POLAND;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 1;
        BigDecimal grossDailyRate = new BigDecimal("119");

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("100")
            .subtract(BigDecimal.valueOf(country.getFixedCost())));
    }

    @Test
    public void shouldCalculateNetAmountWithProperVatForUnitedKingdom() {
        //Given
        Country country = Country.UNITED_KINGDOM;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 1;
        BigDecimal grossDailyRate = new BigDecimal("125");

        mockPolishExchangeRateService(country.getCurrencyCode(), invoiceDate.minusDays(1), BigDecimal.ONE);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("100")
            .subtract(BigDecimal.valueOf(country.getFixedCost())));
    }

    @Test
    public void shouldCalculateNetAmountWithProperVatForGermany() {
        //Given
        Country country = Country.GERMANY;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = 1;
        BigDecimal grossDailyRate = new BigDecimal("120");

        mockPolishExchangeRateService(country.getCurrencyCode(), invoiceDate.minusDays(1), BigDecimal.ONE);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("100")
            .subtract(BigDecimal.valueOf(country.getFixedCost())));
    }

    @Test
    public void shouldGetExchangeRateFromLastWeekdayForSaturday() {
        //Given
        Country country = Country.UNITED_KINGDOM;
        LocalDate saturdayInvoiceDate = LocalDate.of(2018, 3, 24);
        int days = 1;
        BigDecimal grossDailyRate = new BigDecimal("1.25");

        assertThat(saturdayInvoiceDate.getDayOfWeek()).isEqualTo(DayOfWeek.SATURDAY);
        BigDecimal exchangeRate = new BigDecimal("4.8368");
        LocalDate fridayInvoiceDate = saturdayInvoiceDate.minusDays(1);
        mockPolishExchangeRateService(country.getCurrencyCode(), fridayInvoiceDate, exchangeRate);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, saturdayInvoiceDate, days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("4.84")
            .subtract(BigDecimal.valueOf(country.getFixedCost()).multiply(exchangeRate).setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void shouldGetExchangeRateFromLastWeekdayForSunday() {
        //Given
        Country country = Country.UNITED_KINGDOM;
        LocalDate sundayInvoiceDate = LocalDate.of(2018, 3, 25);
        int days = 1;
        BigDecimal grossDailyRate = new BigDecimal("1.25");

        assertThat(sundayInvoiceDate.getDayOfWeek()).isEqualTo(DayOfWeek.SUNDAY);
        BigDecimal exchangeRate = new BigDecimal("4.8368");
        LocalDate fridayInvoiceDate = sundayInvoiceDate.minusDays(2);
        mockPolishExchangeRateService(country.getCurrencyCode(), fridayInvoiceDate, exchangeRate);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, sundayInvoiceDate.plusDays(1), days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("4.84")
            .subtract(BigDecimal.valueOf(country.getFixedCost()).multiply(exchangeRate).setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void shouldGetExchangeRateFromLastWorkDayForBankHolidayInPolandWhichIsInTheMiddleOfWeek() {
        //Given
        Country country = Country.UNITED_KINGDOM;
        LocalDate bankHoliday = LocalDate.of(2017, 8, 15);
        int days = 1;
        BigDecimal grossDailyRate = new BigDecimal("1.25");

        BigDecimal exchangeRate = new BigDecimal("4.7084");
        mockPolishExchangeRateService(country.getCurrencyCode(), bankHoliday.minusDays(1), exchangeRate);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, bankHoliday.plusDays(1), days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("4.71")
            .subtract(BigDecimal.valueOf(country.getFixedCost()).multiply(exchangeRate).setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void shouldGetExchangeRateFromLastWorkDayForBankHolidayInPolandWhichIsOnMonday() {
        //Given
        Country country = Country.GERMANY;
        LocalDate bankHoliday = LocalDate.of(2018, 1, 1);
        int days = 1;
        BigDecimal grossDailyRate = new BigDecimal("1.20");

        BigDecimal exchangeRate = new BigDecimal("4.1709");
        mockPolishExchangeRateService(country.getCurrencyCode(), bankHoliday.minusDays(3), exchangeRate);

        //When
        ResponseEntity<BigDecimal> response = salaryController.calculateNetAmount(country, bankHoliday.plusDays(1), days, grossDailyRate);

        //Then
        assertThat(response.getBody()).isEqualByComparingTo(new BigDecimal("4.17")
            .subtract(BigDecimal.valueOf(country.getFixedCost()).multiply(exchangeRate).setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void shouldThrowExceptionWhenPolishExchangeRateServiceReturnsIncorrectResponse() {
        //Given
        Country country = Country.UNITED_KINGDOM;
        LocalDate invoiceDate = LocalDate.of(2018, 3, 2);
        int days = randomInt();
        BigDecimal grossDailyRate = randomPositiveBigDecimal();

        mockPolishExchangeRateServiceWithWrongResponse(country.getCurrencyCode(), invoiceDate.minusDays(1));

        //When
        Throwable thrown = catchThrowable(() ->
            salaryController.calculateNetAmount(country, invoiceDate, days, grossDailyRate));

        //Then
        assertThat(thrown).hasCauseInstanceOf(ExchangeRateServiceUnavailableException.class);
        assertThat(thrown).hasMessageContaining("exchangeRateService.unavailable");
    }

    private void mockPolishExchangeRateService(String currencyCode, LocalDate exchangeDate, BigDecimal returnExchangeRate) {
        mockServer.expect(
            requestTo(String.format("http://api.nbp.pl/api/exchangerates/rates/a/%s/%s", currencyCode, exchangeDate)))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withSuccess(String.format("{\"rates\":[{\"mid\":%s}]}", returnExchangeRate),
                MediaType.APPLICATION_JSON));
    }

    private void mockPolishExchangeRateServiceWithWrongResponse(String currencyCode, LocalDate exchangeDate) {
        mockServer.expect(
            requestTo(String.format("http://api.nbp.pl/api/exchangerates/rates/a/%s/%s", currencyCode, exchangeDate)))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withSuccess("{\"rates\":[{}]}",
                MediaType.APPLICATION_JSON));
    }
}