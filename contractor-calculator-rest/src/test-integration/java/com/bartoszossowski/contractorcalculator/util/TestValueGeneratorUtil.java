package com.bartoszossowski.contractorcalculator.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

public final class TestValueGeneratorUtil {

    private static Random random = new Random();

    private TestValueGeneratorUtil() {
        throw new AssertionError("Should not be created");
    }

    public static String randomString() {
        return UUID.randomUUID().toString();
    }

    public static LocalDate randomDateInFuture() {
        return LocalDate.now().plusDays(randomPositiveNumber(30));
    }

    public static LocalDate randomDateInPast() {
        return LocalDate.now().minusDays(randomPositiveNumber(30));
    }

    public static long randomLong() {
        return random.nextLong();
    }

    public static int randomInt() {
        return random.nextInt();
    }

    public static int randomPositiveNumber(int bound) {
        return random.nextInt(bound) + 1;
    }

    public static BigDecimal randomPositiveBigDecimal() {
        return BigDecimal.valueOf(randomPositiveNumber(1_000));
    }

}
