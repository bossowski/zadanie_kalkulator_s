package com.bartoszossowski.contractorcalculator.domain;

import com.bartoszossowski.contractorcalculator.config.exception.BadRequestException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class CountryTest {

    @Test
    public void shouldReturnCountryFromCountryCodeString() {
        //When
        Country poland = Country.fromValue("pl");

        //Then
        assertThat(poland).isSameAs(Country.POLAND);
    }

    @Test
    public void shouldThrowExceptionWhenCountryCodeIsNotDefined() {
        //When
        Throwable thrown = catchThrowable(() -> Country.fromValue("xx"));

        //Then
        assertThat(thrown).isInstanceOf(BadRequestException.class);
        assertThat(thrown).hasMessage("country.no-enum");
    }
}