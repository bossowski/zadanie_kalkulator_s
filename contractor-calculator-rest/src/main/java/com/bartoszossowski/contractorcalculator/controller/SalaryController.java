package com.bartoszossowski.contractorcalculator.controller;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.bartoszossowski.contractorcalculator.domain.Country;
import com.bartoszossowski.contractorcalculator.service.SalaryCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Salary controller.
 */
@RestController
@RequestMapping("/salaries")
public class SalaryController {

    private final SalaryCalculator salaryCalculator;

    /**
     * Instantiates a new Salary controller.
     *
     * @param salaryCalculator the salary calculator
     */
    @Autowired
    public SalaryController(SalaryCalculator salaryCalculator) {
        this.salaryCalculator = salaryCalculator;
    }

    /**
     * Calculate net amount response entity.
     *
     * @param country        the country
     * @param invoiceDate    the invoice date
     * @param days           the days
     * @param grossDailyRate the gross daily rate
     * @return the response entity
     */
    @GetMapping("/countries/{country}/invoiceDates/{invoiceDate}/days/{days}/grossDailyRates/{grossDailyRate}")
    public ResponseEntity<BigDecimal> calculateNetAmount(@PathVariable Country country,
                                                         @PathVariable LocalDate invoiceDate,
                                                         @PathVariable int days,
                                                         @PathVariable BigDecimal grossDailyRate) {

        return ResponseEntity.ok(salaryCalculator.calculateNetAmount(country, invoiceDate, days, grossDailyRate));
    }


}
