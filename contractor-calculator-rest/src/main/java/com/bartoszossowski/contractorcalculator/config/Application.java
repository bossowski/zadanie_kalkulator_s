package com.bartoszossowski.contractorcalculator.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * The type Application.
 */
@SpringBootApplication(scanBasePackages = "com.bartoszossowski.contractorcalculator")
@Import({WebConfig.class, CacheConfig.class, AsyncConfig.class})
public class Application {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}