package com.bartoszossowski.contractorcalculator.config.exception;

/**
 * The type Exchange rate service unavailable exception.
 */
public class ExchangeRateServiceUnavailableException extends RuntimeException {

    private static final long serialVersionUID = 8694178294471545826L;

    public ExchangeRateServiceUnavailableException() {
        super("exchangeRateService.unavailable");
    }

}
