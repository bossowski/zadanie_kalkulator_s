package com.bartoszossowski.contractorcalculator.config;

import java.util.concurrent.ThreadFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * The type Async config.
 */
@Configuration
@EnableAsync(proxyTargetClass=true)
class AsyncConfig {

    private static final int CORE_POOL_SIZE = 2;
    private static final int MAX_POOL_SIZE = 10;

    /**
     * Task executor task executor.
     *
     * @return the task executor
     */
    @Bean
    TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        ThreadFactory threadFactory = new CustomizableThreadFactory("TaskExecutor-");
        taskExecutor.setThreadFactory(threadFactory);
        taskExecutor.setCorePoolSize(CORE_POOL_SIZE);
        taskExecutor.setMaxPoolSize(MAX_POOL_SIZE);
        return taskExecutor;
    }
}
