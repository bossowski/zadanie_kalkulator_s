package com.bartoszossowski.contractorcalculator.config.exception;

/**
 * The type Bad request exception.
 */
public class BadRequestException extends RuntimeException {

    /**
     * Instantiates a new Bad request exception.
     *
     * @param messageKey the message key
     */
    public BadRequestException(String messageKey) {
        super(messageKey);
    }
}
