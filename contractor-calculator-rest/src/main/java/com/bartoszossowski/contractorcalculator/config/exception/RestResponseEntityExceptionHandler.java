/*
 * Copyright © 2016 Shodann Ltd.
 * http://shodann.com/
 * All rights reserved.
 */
package com.bartoszossowski.contractorcalculator.config.exception;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String ERROR_MESSAGE_KEY = "errorMessage";

    @ExceptionHandler({IllegalArgumentException.class, BadRequestException.class})
    public void handleBadRequest(RuntimeException e, HttpServletResponse response) throws IOException {
        handleException(e, response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ExchangeRateServiceUnavailableException.class)
    public void handleServiceUnavailable(RuntimeException e, HttpServletResponse response) throws IOException {
        handleException(e.getCause(), response, HttpStatus.SERVICE_UNAVAILABLE);
    }

    private void handleException(Throwable e, HttpServletResponse response, HttpStatus httpStatus)
        throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setStatus(httpStatus.value());

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(ERROR_MESSAGE_KEY, e.getMessage());
        response.getWriter().write(new Gson().toJson(jsonObject));
    }

}
