package com.bartoszossowski.contractorcalculator.config.binder;

import java.beans.PropertyEditorSupport;

import com.bartoszossowski.contractorcalculator.domain.Country;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * The type Country web data binder.
 */
@ControllerAdvice
class CountryWebDataBinder {

    /**
     * Init binder.
     *
     * @param dataBinder the data binder
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(Country.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                setValue(Country.fromValue(text));
            }
        });
    }

}