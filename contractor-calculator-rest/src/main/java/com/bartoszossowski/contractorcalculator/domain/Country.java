package com.bartoszossowski.contractorcalculator.domain;

import java.math.BigDecimal;

import com.bartoszossowski.contractorcalculator.config.exception.BadRequestException;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * The enum Country.
 */
@Getter
@Configurable(preConstruction = true)
public enum Country {

    /**
     * United Kingdom country.
     */
    UNITED_KINGDOM("GB", "GBP", 25, 600),

    /**
     * Germany country.
     */
    GERMANY("DE", "EUR", 20, 800),

    /**
     * Poland country.
     */
    POLAND("PL", "PLN", 19, 1200);

    private static final BigDecimal HUNDRED = new BigDecimal("100");

    private final String countryCode;
    private final String currencyCode;
    private final int vat;
    private final double fixedCost;

//    FIXME: LTW does not want to work in Spring Boot 2.0.0
//    @Autowired
//    private transient PolishExchangeRateService polishExchangeRateService;

    Country(String countryCode, String currencyCode, int vat, double fixedCost) {
        this.countryCode = countryCode;
        this.currencyCode = currencyCode;
        this.vat = vat;
        this.fixedCost = fixedCost;
    }

    /**
     * From value country.
     *
     * @param value the value
     * @return the country
     */
    @JsonCreator
    public static Country fromValue(String value) {
        for (Country country : values()) {
            if (country.getCountryCode().equalsIgnoreCase(value)) {
                return country;
            }
        }

        throw new BadRequestException("country.no-enum");
    }
}
