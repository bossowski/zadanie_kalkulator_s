/*
 * Copyright © 2017 Shodann Ltd.
 * http://shodann.com/
 * All rights reserved.
 */

package com.bartoszossowski.contractorcalculator.service.exchangerate.polish;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * The type Polish exchange rate service response.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Setter
class PolishExchangeRateServiceResponse {
    private List<Rate> rates;

    /**
     * Gets mid rate.
     *
     * @return the mid rate
     */
    BigDecimal getMidRate() {
        return rates.stream()
            .findFirst()
            .map(Rate::getMid)
            .orElseThrow(() -> new IllegalStateException("Missing exchange rate in Polish exchange service response"));
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Setter
    @Getter
    private static class Rate {
        private BigDecimal mid;
    }
}
