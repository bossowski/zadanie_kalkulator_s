/*
 * Copyright © 2017 Shodann Ltd.
 * http://shodann.com/
 * All rights reserved.
 */

package com.bartoszossowski.contractorcalculator.service.exchangerate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.concurrent.CompletableFuture;

/**
 * The interface Exchange rate service.
 */
public interface ExchangeRateService {

    /**
     * Gets exchange rate.
     *
     * @param currencyCode the currency code
     * @param date         the date
     * @return the exchange rate
     */
    CompletableFuture<BigDecimal> getExchangeRate(String currencyCode, LocalDate date);

}
