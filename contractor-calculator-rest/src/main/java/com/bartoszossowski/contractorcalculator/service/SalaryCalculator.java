package com.bartoszossowski.contractorcalculator.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

import com.bartoszossowski.contractorcalculator.config.exception.ExchangeRateServiceUnavailableException;
import com.bartoszossowski.contractorcalculator.domain.Country;
import com.bartoszossowski.contractorcalculator.service.exchangerate.polish.PolishExchangeRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Salary calculator.
 */
@Service
public class SalaryCalculator {

    private static final BigDecimal HUNDRED = new BigDecimal("100");
    private static final Duration EXCHANGE_RATE_SERVICE_TIMEOUT = Duration.ofSeconds(15);

    private final PolishExchangeRateService polishExchangeRateService;

    /**
     * Instantiates a new Salary calculator.
     *
     * @param polishExchangeRateService the polish exchange rate service
     */
    @Autowired
    public SalaryCalculator(PolishExchangeRateService polishExchangeRateService) {
        this.polishExchangeRateService = polishExchangeRateService;
    }

    /**
     * Calculate net amount big decimal.
     *
     * @param country        the country
     * @param invoiceDate    the invoice date
     * @param days           the days
     * @param grossDailyRate the gross daily rate
     * @return the big decimal
     */
    public BigDecimal calculateNetAmount(Country country, LocalDate invoiceDate, int days, BigDecimal grossDailyRate) {
        BigDecimal exchangeRate = polishExchangeRateService
            .getExchangeRate(country.getCurrencyCode(), invoiceDate.minusDays(1))
            .orTimeout(EXCHANGE_RATE_SERVICE_TIMEOUT.getSeconds(), TimeUnit.SECONDS)
            .exceptionally(throwable -> {
                throw new ExchangeRateServiceUnavailableException();
            })
            .join();

        BigDecimal totalGrossAmount = grossDailyRate.multiply(BigDecimal.valueOf(days));
        BigDecimal totalNetAmount = totalGrossAmount.divide(BigDecimal.ONE.add(BigDecimal.valueOf(country.getVat())
            .divide(HUNDRED, 2, RoundingMode.HALF_UP)), 2, RoundingMode.HALF_UP);
        return totalNetAmount.subtract(BigDecimal.valueOf(country.getFixedCost()))
            .multiply(exchangeRate).setScale(2, RoundingMode.HALF_UP);
    }
}
