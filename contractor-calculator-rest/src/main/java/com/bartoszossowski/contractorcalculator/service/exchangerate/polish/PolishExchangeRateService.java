/*
 * Copyright © 2017 Shodann Ltd.
 * http://shodann.com/
 * All rights reserved.
 */

package com.bartoszossowski.contractorcalculator.service.exchangerate.polish;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.concurrent.CompletableFuture;

import com.bartoszossowski.contractorcalculator.domain.Country;
import com.bartoszossowski.contractorcalculator.service.exchangerate.ExchangeRateService;
import de.jollyday.HolidayCalendar;
import de.jollyday.HolidayManager;
import de.jollyday.ManagerParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * The type Polish exchange rate service.
 */
@Service
public class PolishExchangeRateService implements ExchangeRateService {

    private static final String URL = "http://api.nbp.pl/api/exchangerates/rates/a/{code}/{date}";
    private static final HolidayManager HOLIDAY_MANAGER
        = HolidayManager.getInstance(ManagerParameters.create(HolidayCalendar.POLAND));

    private final RestTemplate restTemplate;

    @Autowired
    public PolishExchangeRateService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @Async
    @Cacheable("exchangeRateServiceCache")
    public CompletableFuture<BigDecimal> getExchangeRate(String currencyCode, LocalDate date) {

        if (currencyCode.equalsIgnoreCase(Country.POLAND.getCurrencyCode())) {
            return CompletableFuture.completedFuture(BigDecimal.ONE);
        }

        return CompletableFuture.completedFuture(
            restTemplate.getForEntity(URL, PolishExchangeRateServiceResponse.class, currencyCode, getLastWorkDay(date))
                .getBody().getMidRate());
    }

    private LocalDate getLastWorkDay(LocalDate date) {
        LocalDate workDay = date;
        while (HOLIDAY_MANAGER.isHoliday(workDay) || workDay.getDayOfWeek() == DayOfWeek.SATURDAY
            || workDay.getDayOfWeek() == DayOfWeek.SUNDAY) {
            workDay = workDay.minusDays(1);
        }

        return workDay;
    }
}
